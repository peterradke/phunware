// INetworkCallback.aidl
package com.phunware.evaluation;

// Declare any non-default types here with import statements
//import com.phunware.evaluation.datamodels.Venue;
import datamodels.Venue;
interface INetworkCallback {
     void feedDownloaded(in List<Venue> venues);
}
