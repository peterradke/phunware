// INetworkService.aidl
package com.phunware.evaluation;

// Declare any non-default types here with import statements
import com.phunware.evaluation.INetworkCallback;
interface INetworkService {
    void getFeed();

    void addCallback(INetworkCallback callback);
}
