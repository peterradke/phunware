package com.phunware.evaluation;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import datamodels.Feed;
import datamodels.Venue;
import fragments.BaseFragment;
import fragments.DetailsFragment;
import fragments.ListFragment;
import util.NetworkService;

/**
 * © Peter Radke 20 Mar 2015
 */
public class MainActivity extends ActionBarActivity implements BaseFragment.FragmentInterface {

    private static String TAG = MainActivity.class.getSimpleName();

    private ListFragment mListFragment = new ListFragment();

    private DetailsFragment mDetailsFragment = new DetailsFragment();

    private boolean mIsTablet;

    private INetworkService mINetworkService;

    private MenuItem mShareButton;

    private boolean mIsBoundToService;

    private ShareActionProvider mShareActionProvider;

    private ProgressBar mProgressBar;

    private ServiceConnection mConnection = new ServiceConnection() {
        // Called when the connection with the service is established
        public void onServiceConnected(ComponentName className, IBinder service) {
            // this gets an instance of the IRemoteInterface, which we can use to call on the service
            mINetworkService = INetworkService.Stub.asInterface(service);
            mIsBoundToService = true;
            try {
                mINetworkService.addCallback(mCallback);
                if (Feed.getInstance().getVenues() == null) {
                    setProgressBarVisibility(View.VISIBLE);
                    mINetworkService.getFeed();
                }
            } catch (RemoteException e) {
                e.printStackTrace();
                mIsBoundToService = false;
            }
        }

        // Called when the connection with the service disconnects unexpectedly
        public void onServiceDisconnected(ComponentName className) {
            mINetworkService = null;
        }
    };


    private final INetworkCallback mCallback = new INetworkCallback.Stub() {
        @Override
        public void feedDownloaded(List<Venue> venues) throws RemoteException {
            Feed feed = Feed.getInstance();
            feed.setVenues(venues);
            setProgressBarVisibility(View.GONE);
            swapFragment(mListFragment, false);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressBar = (ProgressBar) findViewById(R.id.list_loading_progress_bar);
        mIsTablet = getResources().getBoolean(R.bool.has_two_panes);
        setActionBarTitle(getResources().getString(R.string.action_title_default));
    }

    @Override
    protected void onResume() {
        if (isNetworkAvailable()) {
            Intent intent = new Intent(getApplicationContext(), NetworkService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
        // This can be used to start the service as an intentservice and pass an intent into it.
        // The intent would hold a value of the network function to perform.
        // Intent i = new Intent(getApplicationContext(), NetworkService.class);
        // startService(i);
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (mIsBoundToService) {
            unbindService(mConnection);
            mIsBoundToService = false;
        }
        super.onPause();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mShareButton = menu.findItem(R.id.menu_share);

        // Fetch and store ShareActionProvider
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(mShareButton);
        setShareIntent(new Venue().getShareIntent());

        // Return true to display menu
        return super.onCreateOptionsMenu(menu);
    }

    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            createNetErrorDialog();
            return false;
        }
    }

    /**
     * called to create a dialog fragment to prompt the user to turn on network settings
     */
    protected void createNetErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.alert_network_message))
                .setTitle(getResources().getString(R.string.alert_network_title))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.alert_network_positive),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                startActivity(i);
                            }
                        }
                )
                .setNegativeButton(getResources().getString(R.string.alert_network_negative),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                MainActivity.this.finish();
                            }
                        }
                );
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * changes the progress bar visibility on the UI Thread
     *
     * @param show is the int for GONE, VISIBLE, INVISIBLE
     */
    private void setProgressBarVisibility(final int show) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressBar != null) {
                    mProgressBar.setVisibility(show);
                }
            }
        });

    }

    /**
     * Handles the fragment replacement with the fragment manager. Handles calls differently based
     * on if it is a tablet device or handset. This function helps to make the app scalable if
     * there
     * were many more fragments to swap in and out.
     *
     * © Peter Radke 20 Mar 2015
     *
     * @param fragment       is the fragment to add to the main container
     * @param addToBackStack is a boolean that will add the action to the backstack or not
     */
    private void swapFragment(Fragment fragment, boolean addToBackStack) {
        int container = R.id.layout_fragment_container;
        if (mIsTablet) {
            if (fragment == mListFragment) {
                container = R.id.layout_fragment_master;
            } else if (fragment == mDetailsFragment) {
                fragment = DetailsFragment.clone(mDetailsFragment);
            }
        }

        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();
        ft.replace(container, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            ft.addToBackStack(fragment.getClass().getName());
        }
        ft.commitAllowingStateLoss();
    }

    /**
     * called from a fragment to indicate that the user has selected a venue from the list
     *
     * @param venue holds the selected venue
     */
    @Override
    public void onVenueSelected(Venue venue) {
        mDetailsFragment.setVenue(venue);
        setShareIntent(venue.getShareIntent());
        swapFragment(mDetailsFragment, true);
    }

    /**
     * called from a fragment to show or hide the share button on the action bar.
     *
     * @param showButton boolean true to show, false to hide
     */
    @Override
    public void showShareButton(boolean showButton) {
        if (!mIsTablet) {
            mShareButton.setVisible(showButton);
        }
    }

    @Override
    public void setActionBarTitle(String string) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(string);
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() < 1) {
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}
