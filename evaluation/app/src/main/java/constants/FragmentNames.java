package constants;

/**
 * Created by peterradke on 3/21/15.
 * © Peter Radke 21 Mar 2015
 */

import android.os.Parcel;
import android.os.Parcelable;

public enum FragmentNames implements Parcelable {
    SPLASH_SCREEN,
    LIST_FRAGMENT,
    DETAILS_FRAGMENT;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeInt(ordinal());
    }

    public static final Creator<FragmentNames> CREATOR = new Creator<FragmentNames>() {
        @Override
        public FragmentNames createFromParcel(final Parcel source) {
            return FragmentNames.values()[source.readInt()];
        }

        @Override
        public FragmentNames[] newArray(final int size) {
            return new FragmentNames[size];
        }
    };
}
