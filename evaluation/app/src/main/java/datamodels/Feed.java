package datamodels;

import java.util.List;

/**
 * This class is a singleton reference to the feed of venues that is pulled from the AWS server
 * Created by peterradke on 3/21/15.
 *  © Peter Radke 21 Mar 2015
 */
public class Feed {

    private static Feed sInstance;

    private List<Venue> mVenues;

    public static Feed getInstance() {
        if (sInstance == null) {
            sInstance = new Feed();
        }
        return sInstance;
    }

    private Feed() {
    }

    public void setVenues(List<Venue> venues) {
        sInstance.mVenues = venues;
    }

    public List<Venue> getVenues() {
        return sInstance.mVenues;
    }
}
