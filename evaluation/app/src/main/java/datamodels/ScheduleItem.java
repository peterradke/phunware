package datamodels;

/**
 *
 * Created by peterradke on 3/20/15.
 * © Peter Radke 20 Mar 2015
 *
 * The ScheduleItem object holds the data for the schedules and handles the formatting.
 *
 * Some functions are unused. They were created from the sample model. These were left for future scalability.
 */


import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ScheduleItem implements Parcelable {

    private static final SimpleDateFormat FORMATTER_PARSE = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss Z", Locale.US);

    private static final SimpleDateFormat FORMATTER_START = new SimpleDateFormat("EEEE M/dd h:mma",
            Locale.US);

    private static final SimpleDateFormat FORMATTER_END = new SimpleDateFormat("h:mma", Locale.US);

    private Date mStartDate;

    private Date mEndDate;


    public ScheduleItem() {

    }

    public ScheduleItem(Date startDate, Date endDate) {
        mStartDate = startDate;
        mEndDate = endDate;
    }

    public ScheduleItem(String startDate, String endDate) throws ParseException {
        mStartDate = FORMATTER_PARSE.parse(startDate);
        mEndDate = FORMATTER_PARSE.parse(endDate);
    }

    public Date getStartDate() {
        return mStartDate;
    }

    public void setStartDate(Date startDate) {
        mStartDate = startDate;
    }

    public Date getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Date endDate) {
        mEndDate = endDate;
    }

    public void setStartDate(String startDate) throws ParseException {
        mStartDate = FORMATTER_PARSE.parse(startDate);
    }

    public void setEndDate(String endDate) throws ParseException {
        mEndDate = FORMATTER_PARSE.parse(endDate);
    }

    public String getStartDateString() {
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
        symbols.setAmPmStrings(new String[]{"am", "pm"});
        FORMATTER_START.setDateFormatSymbols(symbols);
        return FORMATTER_START.format(mStartDate);

    }

    public String getEndDateString() {
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
        symbols.setAmPmStrings(new String[]{"am", "pm"});
        FORMATTER_END.setDateFormatSymbols(symbols);
        return FORMATTER_END.format(mEndDate);
    }

    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof ScheduleItem) {
            result = mStartDate.equals(((ScheduleItem) o).getStartDate())
                    && mEndDate.equals(((ScheduleItem) o).getEndDate());
        }
        return result;
    }

    @Override
    public int hashCode() {
        String s = getStartDateString() + getEndDateString();
        return s.hashCode();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeSerializable(mStartDate);
        parcel.writeSerializable(mEndDate);
    }

    public ScheduleItem(Parcel in) {
        mStartDate = (Date) in.readSerializable();
        mEndDate = (Date) in.readSerializable();
    }

    public static final Parcelable.Creator<ScheduleItem>
            CREATOR = new Parcelable.Creator<ScheduleItem>() {
        public ScheduleItem createFromParcel(Parcel in) {
            return new ScheduleItem(in);
        }

        public ScheduleItem[] newArray(int size) {
            return new ScheduleItem[size];
        }
    };
}