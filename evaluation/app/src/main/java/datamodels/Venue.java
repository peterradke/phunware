package datamodels;

/**
 * Created by peterradke on 3/20/15.
 * © Peter Radke 20 Mar 2015
 *
 * The Venue object holds the data for the venues. This object also contains the ability to download the image found online at mImageUrl.
 *
 * Some functions are unused. They were created from the sample model. These were left for future scalability.
 */

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;
import java.util.List;

import util.ImageDownloader;

public class Venue implements Parcelable {

    // Core fields
    private long mId;

    private int mPcode;

    private double mLatitude;

    private double mLongitude;

    private String mName;

    private String mAddress;

    private String mCity;

    private String mState;

    private String mZip;

    private String mPhone;

    // Super Bowl venue fields
    private String mTollFreePhone;

    private String mUrl;

    private String mDescription;

    private String mTicketLink;

    private String mImageUrl;

    private List<ScheduleItem> mSchedule;

    // computed fields
    private float mDistance;

    private byte[] mBitmapBytes;

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getTicketLink() {
        return mTicketLink;
    }

    public void setTicketLink(String ticketLink) {
        mTicketLink = ticketLink;
    }

    public List<ScheduleItem> getSchedule() {
        return mSchedule;
    }

    public void setSchedule(List<ScheduleItem> schedule) {
        mSchedule = schedule;
    }

    public String getTollFreePhone() {
        return mTollFreePhone;
    }

    public void setTollFreePhone(String tollFreePhone) {
        mTollFreePhone = tollFreePhone;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public Venue() {

    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getZip() {
        return mZip;
    }

    public void setZip(String zip) {
        mZip = zip;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public float getDistance() {
        return mDistance;
    }

    public void setDistance(float distance) {
        mDistance = distance;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Venue && ((Venue) o).getId() == mId) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Long.valueOf(mId).hashCode();
    }

    public int getPcode() {
        return mPcode;
    }

    public void setPcode(int pcode) {
        mPcode = pcode;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public Bitmap getImage() {
        Bitmap img = null;
        if (mBitmapBytes != null) {
            img = BitmapFactory.decodeByteArray(mBitmapBytes, 0, mBitmapBytes.length);
        }
        return img;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getFormatedAddress(boolean multiLine) {
        return getAddress() + (multiLine ? "\r\n" : " ") + getState() + ", " + getCity() + " "
                + getZip();
    }

    public String getFormattedHours(boolean multiLine) {
        String returnString = "";
        String separator = (multiLine ? "\r\n" : " ");
        for (ScheduleItem schedule : getSchedule()) {
            returnString += schedule.getStartDateString() + " to " + schedule
                    .getEndDateString() + separator;
        }
        return returnString;
    }

    /**
     * Creates an ImageDownloader object and downloads the image using mImageUrl.
     * © Peter Radke 20 Mar 2015
     *
     * @param callback is passed in and used to notify the caller when the image is downloaded.
     */
    public void downloadImage(final ImageDownloader.Callback callback) {
        if (mBitmapBytes != null) {
            callback.downloadSuccesful(mBitmapBytes);
        } else {
            ImageDownloader imgDownloader = new ImageDownloader(getImageUrl(),
                    new ImageDownloader.Callback() {
                        @Override
                        public void downloadSuccesful(byte[] image) {
                            mBitmapBytes = image;
                            callback.downloadSuccesful(image);
                        }

                        @Override
                        public void downloadFailed(String error) {
                            callback.downloadFailed(error);
                        }
                    });
            imgDownloader.downloadImage();
        }
    }

    /**
     * creates the intent to be shared when the share button is pressed
     */
    public Intent getShareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);

        intent.setType("text/plain");
        String message = getName() + "\r\n" + getFormatedAddress(true);
        intent.putExtra(Intent.EXTRA_TEXT, message);

        return intent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeLong(mId);
        parcel.writeInt(mPcode);
        parcel.writeDouble(mLatitude);
        parcel.writeDouble(mLongitude);
        parcel.writeString(mName);
        parcel.writeString(mAddress);
        parcel.writeString(mCity);
        parcel.writeString(mState);
        parcel.writeString(mZip);
        parcel.writeString(mPhone);
        parcel.writeString(mTollFreePhone);
        parcel.writeString(mUrl);
        parcel.writeString(mDescription);
        parcel.writeString(mTicketLink);
        parcel.writeString(mImageUrl);
        parcel.writeFloat(mDistance);
        parcel.writeList(mSchedule);
        parcel.writeByteArray(mBitmapBytes);
    }

    public Venue(Parcel in) {

        mId = in.readLong();
        mPcode = in.readInt();
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
        mName = in.readString();
        mAddress = in.readString();
        mCity = in.readString();
        mState = in.readString();
        mZip = in.readString();
        mPhone = in.readString();
        mTollFreePhone = in.readString();
        mUrl = in.readString();
        mDescription = in.readString();
        mTicketLink = in.readString();
        mImageUrl = in.readString();
        mDistance = in.readFloat();
        mSchedule = in.readArrayList(ScheduleItem.class.getClassLoader());
        int length = in.readInt();
        if (length > -1) {
            mBitmapBytes = new byte[length];
            in.readByteArray(mBitmapBytes);
        }
    }

    public static final Parcelable.Creator<Venue>
            CREATOR = new Parcelable.Creator<Venue>() {
        public Venue createFromParcel(Parcel in) {
            return new Venue(in);
        }

        public Venue[] newArray(int size) {
            return new Venue[size];
        }
    };

    /**
     * used to sort the venues by distance
     */
    public static Comparator<Venue> VenueComparator = new Comparator<Venue>() {
        @Override
        public int compare(Venue venue1, Venue venue2) {
            Integer stationName1 = (int) venue1.getDistance();
            Integer stationName2 = (int) venue2.getDistance();
            return stationName1.compareTo(stationName2);
        }
    };

    /**
     * used to sort venues by alphabetical order
     */
    public static Comparator<Venue> VenueComparatorAlphabet = new Comparator<Venue>() {
        @Override
        public int compare(Venue venue1, Venue venue2) {
            String stationName1 = venue1.getName();
            String stationName2 = venue2.getName();
            return stationName1.compareTo(stationName2);
        }
    };

}