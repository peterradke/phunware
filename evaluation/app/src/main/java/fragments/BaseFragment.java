package fragments;


import android.app.Activity;
import android.support.v4.app.Fragment;

import constants.FragmentNames;
import datamodels.Venue;

/**
 * Created by peterradke on 1/5/15.
 * © Peter Radke 5 Jan 2015
 */
public class BaseFragment extends Fragment {

    private static final String ERROR_MESSAGE = " must implement BaseFragment.FragmentInterface";

    private FragmentNames mFragmentName;

    protected FragmentInterface mInterface;

    public FragmentNames getFragmentName() {
        return mFragmentName;

    }
    /**
     * Created by peterradke on 1/5/15.
     * © Peter Radke 5 Jan 2015
     */
    public void setFragmentName(FragmentNames fragmentName) {
        mFragmentName = fragmentName;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mInterface = (FragmentInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + ERROR_MESSAGE);
        }
    }

    public interface FragmentInterface {

        void onVenueSelected(Venue item);

        void showShareButton(boolean showButton);

        void setActionBarTitle(String string);
    }
}
