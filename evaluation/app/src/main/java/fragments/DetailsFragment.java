package fragments;

import com.phunware.evaluation.R;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import constants.FragmentNames;
import datamodels.Feed;
import datamodels.Venue;
import util.ImageDownloader;

/**
 * Created by peterradke on 3/21/15.
 * © Peter Radke 21 Mar 2015
 */
public class DetailsFragment extends BaseFragment {

    private static String TAG = ListFragment.class.getSimpleName();

    private Feed mFeed = Feed.getInstance();

    private ImageView mImage;

    private TextView mTextName;

    private TextView mTextAddress;

    private TextView mTextHours;

    private Venue mVenue;

    private ProgressBar mProgressBar;

    public DetailsFragment() {
        setFragmentName(FragmentNames.DETAILS_FRAGMENT);
    }

    public static DetailsFragment clone(DetailsFragment copy) {
        DetailsFragment clone = new DetailsFragment();
        clone.setVenue(copy.mVenue);
        return clone;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
        if (mInterface != null) {
            mInterface.showShareButton(true);
            mInterface.setActionBarTitle(
                    getActivity().getResources().getString(R.string.action_title_details));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_fragment, container, false);

        mImage = (ImageView) view.findViewById(R.id.image_details);
        mTextName = (TextView) view.findViewById(R.id.text_details_name);
        mTextAddress = (TextView) view.findViewById(R.id.text_details_address);
        mTextHours = (TextView) view.findViewById(R.id.text_details_hours);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        /**
         * Created by peterradke on 1/5/15.
         * © Peter Radke 5 Jan 2015
         */
        if (mVenue != null) {
            mTextName.setText(mVenue.getName());
            mTextAddress.setText(mVenue.getFormatedAddress(true));
            mTextHours.setText(mVenue.getFormattedHours(true));
            if (mVenue.getImage() != null) {

                mImage.setImageBitmap(mVenue.getImage());
            } else {
                if (mVenue.getImageUrl() != null) {
                    if (mVenue.getImageUrl().compareToIgnoreCase("") != 0) {
                        mProgressBar.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        return view;
    }

    /**
     * Created by peterradke on 1/5/15.
     * © Peter Radke 5 Jan 2015
     */
    private void showImage() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mProgressBar != null) {
                        mProgressBar.setVisibility(View.GONE);
                    }
                    if (mImage != null) {
                        mImage.setImageBitmap(mVenue.getImage());
                    }
                }
            });
        }
    }

    /**
     * Created by peterradke on 1/5/15.
     * © Peter Radke 5 Jan 2015
     */
    public void setVenue(final Venue venue) {
        mVenue = venue;
        if (mVenue.getImageUrl() != null) {
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.VISIBLE);
            }
            mVenue.downloadImage(new ImageDownloader.Callback() {
                @Override
                public void downloadSuccesful(byte[] image) {
                    showImage();
                }

                @Override
                public void downloadFailed(String error) {

                }
            });
        }
    }
}
