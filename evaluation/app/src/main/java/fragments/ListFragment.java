package fragments;

import com.phunware.evaluation.R;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Comparator;

import constants.FragmentNames;
import datamodels.Feed;
import datamodels.Venue;

/**
 * Created by peterradke on 3/21/15.
 * © Peter Radke 21 Mar 2015
 */
public class ListFragment extends BaseFragment {

    private static String TAG = ListFragment.class.getSimpleName();

    private Feed mFeed = Feed.getInstance();

    private VenuesListAdapter mAdapter;

    private ListView mListView;

    private static final String LIST_STATE = "list_state";

    private Parcelable mListState = null;

    public ListFragment() {
        setFragmentName(FragmentNames.LIST_FRAGMENT);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mListView != null) {
            mListState = mListView.onSaveInstanceState();
        }
        outState.putParcelable(LIST_STATE, mListState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        mListView.onSaveInstanceState();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
        if (mInterface != null) {
            mInterface.showShareButton(false);
        }
        if (mListState != null) {
            mListView.onRestoreInstanceState(mListState);
            mInterface.setActionBarTitle(
                    getActivity().getResources().getString(R.string.action_title_default));
        }
        mListState = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);

        mListView = (ListView) view.findViewById(R.id.list_master);
        if (mAdapter == null) {
            mAdapter = new VenuesListAdapter(getActivity());
            if (mFeed != null) {
                for (Venue venue : mFeed.getVenues()) {
                    mAdapter.add(venue);
                }
            }
        }
        mAdapter.notifyDataSetChanged();
        mListView.setChoiceMode(1);
        mListView.setOnItemClickListener(mAdapterClickedHandler);
        mListView.setAdapter(mAdapter);

        return view;
    }


    private class VenuesListAdapter extends ArrayAdapter<Venue> {

        public VenuesListAdapter(Context context) {
            super(context, R.layout.venue_list_item);
        }

        private class ViewHolder {

            private TextView pTextName;

            private TextView pTextAddress;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = new ViewHolder();

            if (convertView == null) {
                LayoutInflater inflater = getActivity().getLayoutInflater();
                convertView = inflater.inflate(R.layout.venue_list_item, parent, false);

                holder.pTextName = (TextView) convertView.findViewById(R.id.text_name);
                holder.pTextAddress = (TextView) convertView.findViewById(R.id.text_address);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if (getItem(position).getName() != null) {
                holder.pTextName.setText(getItem(position).getName());
            }
            if (getItem(position).getAddress() != null) {
                holder.pTextAddress.setText(getItem(position).getFormatedAddress(false));
            }

            return convertView;
        }

        /*
         * @see android.widget.ArrayAdapter#sort(java.util.Comparator)
         */
        @Override
        public void sort(Comparator<? super Venue> comparator) {
            super.sort(Venue.VenueComparatorAlphabet);
        }

    }

    /*
     * Handles the clicking on of the ListItem row
     */
    private AdapterView.OnItemClickListener mAdapterClickedHandler
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            view.setSelected(true);
            mAdapter.notifyDataSetChanged();
            mInterface.onVenueSelected(mAdapter.getItem(position));
        }
    };
}
