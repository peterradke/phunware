package util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import datamodels.ScheduleItem;
import datamodels.Venue;

/**
 * Created by peterradke on 3/20/15.
 * © Peter Radke 20 Mar 2015
 */
public class ApiBuilder extends NetworkHelper {

    private static String TAG = ApiBuilder.class.getSimpleName();

    private static String FEED_URL
            = "https://s3.amazonaws.com/jon-hancock-phunware/nflapi-static.json";

    private static String ADDRESS = "address";

    private static String CITY = "city";

    private static String DESCRIPTION = "description";

    private static String ID = "id";

    private static String IMAGE_URL = "image_url";

    private static String LATITUDE = "latitude";

    private static String LONGITUDE = "longitude";

    private static String NAME = "name";

    private static String P_CODE = "pcode";

    private static String PHONE = "phone";

    private static String STATE = "state";

    private static String TICKET_LINK = "ticket_link";

    private static String TOLL_FREE_PHONE = "tollfreephone";

    private static String ZIP = "zip";

    private static String SCHEDULE = "schedule";

    private static String START_DATE = "start_date";

    private static String END_STATE = "end_date";

    public ArrayList<Venue> getFeed() {
        String responseString;
        ArrayList<Venue> venues = new ArrayList<Venue>();
        try {
            responseString = getResponse(FEED_URL);
            JSONArray resultsArray = new JSONArray(responseString);
            for (int i = 0; i < resultsArray.length(); i++) {
                JSONObject obj = resultsArray.getJSONObject(i);

                Venue venue = new Venue();
                venue.setAddress(obj.has(ADDRESS) ? obj.getString(ADDRESS) : null);
                venue.setCity(obj.has(CITY) ? obj.getString(CITY) : null);
                venue.setDescription(obj.has(DESCRIPTION) ? obj.getString(DESCRIPTION) : null);
                venue.setId(obj.has(ID) ? obj.getInt(ID) : venue.getId());
                venue.setImageUrl(obj.has(IMAGE_URL) ? obj.getString(IMAGE_URL) : null);
                venue.setLatitude(
                        obj.has(LATITUDE) ? obj.getDouble(LATITUDE) : venue.getLatitude());
                venue.setLongitude(
                        obj.has(LONGITUDE) ? obj.getDouble(LONGITUDE) : venue.getLongitude());
                venue.setName(obj.has(NAME) ? obj.getString(NAME) : null);
                venue.setPcode(obj.has(P_CODE) ? obj.getInt(P_CODE) : venue.getPcode());
                venue.setPhone(obj.has(PHONE) ? obj.getString(PHONE) : null);
                venue.setState(obj.has(STATE) ? obj.getString(STATE) : null);
                venue.setTicketLink(obj.has(TICKET_LINK) ? obj.getString(TICKET_LINK) : null);
                venue.setTollFreePhone(
                        obj.has(TOLL_FREE_PHONE) ? obj.getString(TOLL_FREE_PHONE) : null);
                venue.setZip(obj.has(ZIP) ? obj.getString(ZIP) : null);

                if (obj.has(SCHEDULE)) {
                    JSONArray scheduleArray = obj.getJSONArray(SCHEDULE);
                    List<ScheduleItem> scheduleItems = new ArrayList<ScheduleItem>();
                    for (int j = 0; j < scheduleArray.length(); j++) {
                        ScheduleItem item = new ScheduleItem();
                        JSONObject scheduleObj = scheduleArray.getJSONObject(j);
                        item.setStartDate(
                                scheduleObj.has(START_DATE) ? scheduleObj.getString(START_DATE)
                                        : null);
                        item.setEndDate(
                                scheduleObj.has(END_STATE) ? scheduleObj.getString(END_STATE)
                                        : null);
                        scheduleItems.add(item);
                    }
                    venue.setSchedule(scheduleItems);
                }
                venues.add(venue);
            }
            return venues;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
