package util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by peterradke on 3/22/15.
 * © Peter Radke 22 Mar 2015
 *
 * This is used to download an image. I would like to add in a form of image caching
 */
public class ImageDownloader {

    private String mUrl;

    private Callback mCallback;

    public interface Callback {

        void downloadSuccesful(byte[] image);

        void downloadFailed(String error);
    }

    public ImageDownloader(String url, Callback callback) {
        mCallback = callback;
        mUrl = url;
    }
    /**
     * Created by peterradke on 1/5/15.
     * © Peter Radke 5 Jan 2015
     */
    public void downloadImage() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap mImg = null;
                try {
                    InputStream in = new URL(mUrl).openStream();
                    mImg = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                if (mImg != null) {
                    mImg.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] bytes = stream.toByteArray();
                    mCallback.downloadSuccesful(bytes);
                } else {
                    mCallback.downloadFailed("Cannot download...." + mUrl);
                }
            }
        }).start();
    }

}
