package util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by peterradke on 3/20/15.
 * © Peter Radke 20 Mar 2015
 *
 * Used to handle network requests
 */
public class NetworkHelper {


    private static final int TIME_OUT = 5000;

    protected static String getResponse(String strUrl)
            throws IOException {

        URL url = new URL(strUrl);

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setDoInput(true);

        urlConnection.setUseCaches(true);
        urlConnection.setConnectTimeout(TIME_OUT);
        urlConnection.connect();
        try {
            // grab the InputStream input from the URL and convert it to a string
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            // body holds the human readable JSON string that returned from the server
            String body = convertStreamToString(in);
            return body;
        } finally {
            // disconnect the connection
            urlConnection.disconnect();
        }
    }

    /*
     * This function converts an InputStream into human readable String format
     */
    private static String convertStreamToString(InputStream is) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
        StringBuilder stringBuilder = new StringBuilder();

        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
        } catch (Exception ex) {
            return null;
        } finally {
            try {
                is.close();
            } catch (Exception ex) {
                return null;
            }
        }
        return stringBuilder.toString();
    }
}
