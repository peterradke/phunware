package util;

import com.phunware.evaluation.INetworkCallback;
import com.phunware.evaluation.INetworkService;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.List;

import datamodels.Feed;

/**
 * Created by peterradke on 3/20/15.
 * © Peter Radke 20 Mar 2015
 */
public class NetworkService extends IntentService {

    private static String TAG = NetworkService.class.getSimpleName();

    private List<INetworkCallback> mCallbacks = new ArrayList<INetworkCallback>();

    private final INetworkService.Stub mBinder = new INetworkService.Stub() {

        @Override
        public void getFeed() throws RemoteException {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    downloadFeed();
                }
            }).start();
        }

        @Override
        public void addCallback(INetworkCallback callback) throws RemoteException {
            mCallbacks.add(callback);
        }
    };

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public NetworkService(String name) {
        super("NetworkService");
    }

    public NetworkService() {
        super("NetworkService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        downloadFeed();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Created by peterradke on 3/20/15.
     * © Peter Radke 20 Mar 2015
     */
    private void downloadFeed() {
        ApiBuilder api = new ApiBuilder();

        Feed feed = Feed.getInstance();
        if (feed.getVenues() == null) {
            feed.setVenues(api.getFeed());
        }

        for (INetworkCallback callback : mCallbacks) {
            try {
                callback.feedDownloaded(feed.getVenues());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
